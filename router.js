const Authentication = require('./controllers/authentication');
const ProductController = require('./controllers/productController');
const passportService = require('./services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

module.exports = function (app) {
  app.post('/login', requireSignin, Authentication.login);
  app.post('/signup', Authentication.signup);

  app.get('/products', ProductController.getProducts);
  app.post('/products', requireAuth, ProductController.addProduct);
  app.put('/products/:id', requireAuth, ProductController.updateProduct);
};
