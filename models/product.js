const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define our model
const userSchema = new Schema({
  name: String,
  stock: Number,
  price: Number,
  shortDesc: String,
  description: String,
});

// Create the model class
const ModelClass = mongoose.model('product', userSchema);

// Export the model
module.exports = ModelClass;
