const Product = require('../models/product');

exports.getProducts = function (req, res, next) {
  Product.find({}, function (err, products) {
    if (err) {
      return next(err);
    }

    res.send(products);
  });
};

exports.addProduct = function (req, res, next) {
  const name = req.body.name;
  const price = req.body.price;
  const stock = req.body.stock;
  const shortDesc = req.body.shortDesc;
  const description = req.body.description;

  const product = new Product({
    name: name,
    price: price,
    stock: stock,
    shortDesc: shortDesc,
    description: description,
  });

  product.save(function (err) {
    if (err) {
      return next(err);
    }

    res.json({ status: 'Product added successfully' });
  });
};

exports.updateProduct = function (req, res, next) {
  const id = req.params.id;
  const stock = req.body.stock;

  Product.findByIdAndUpdate(
    id,
    { stock: stock },
    function (err, existingProduct) {
      if (err) {
        return next(err);
      }

      res.json({ status: 'Product updated successfully' });
    },
  );
};
